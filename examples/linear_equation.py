from defworld.agent import Agent
from defworld.basic import Var
from defworld.patterns import Entity, Func, Not, Rule, Template

if __name__ == '__main__':
    solution = Template('solution', Entity)

    a = Var('a')
    b = Var('b')
    expr = Var('expr')
    x = Entity('unknown')

    product = Template('product')
    add = Template('add')

    equation = Template('equation', Entity)

    @Func
    def Div(a, b):
        return a/b

    @Func
    def Sub(a, b):
        return a-b

    @Func
    def solved(x):
        print x.value
        return solution(x)

    agent = Agent([
            equation(add(product(2,x), 3), 5)      # 2x + 3 = 5
        ],[
            Rule('x = b', [
                    Not(solution()),  # not yet solved
                    equation(x, b)    # x = b
                ],[
                    solved(b)         # the solution is b
                ]),

            Rule('a * expr = b', [
                    Not(solution()),               # not yet solved
                    equation(product(a, expr), b)  # a * expr = b
                ],[
                    equation(expr, Div(b, a))      # expr = b / a
                ]),

            Rule('expr + a = b', [
                    Not(solution()),           # not yet solved
                    equation(add(expr, a), b)  # expr + a = b
                ],[
                    equation(expr, Sub(b, a))  # expr = b - a
                ]),
        ])
    agent.match()
    agent.run() # print 1
