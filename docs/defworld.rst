Defworld
========

Defworld is a multiagent simulation library for interactive storytelling.

Rule
^^^^

Each agent manipulates ideas on its working memory(WM) according to a set of
IF-THEN style rules. For example, the following code means "If Socrates is a
human, then Socrates is mortal".

.. sourcecode:: python

    Rule('socrates-mortal', [         # A 'socrates-mortal' rule:
        Fact('Socrates is a human'),  # If Socrates is a human,
        ],[                           # then
        Fact('Socrates is mortal')])  # Socrates is mortal.

Fact is a WM item about facts.  The 'socrates-mortal' rule append a fact
"Socrates is mortal" on the WM when there is a fact "Sorcrates is a human" on
WM.

On the other hand, some rules executes actions. For example, the follwing code
means "If the phone rings, answer the phone"

.. sourcecode:: python

    Rule('answer-phone', [            # A 'answer-phone' rule:
        Fact('the phone rings'),      # If the phone rings,
        ],[                           # then
        Action('answer the phone')])  # answer the phone

Action is also a WM item. This 'answer-phone' rule append the Action, 'answer
the phone' on WM, and then ActionModule executes the action in the designated
way.

Every WM item has a name and slots. For example,

.. sourcecode:: python

    Fact('human', name='Socrates')
    Action('answer', target='phone')

Slot values could be variables. Defworld fills these variables with pattern
matching.

.. sourcecode:: python

    Rule('human-mortal', [              # A 'human-mortal' rule:
        Fact('human', name=Var('x')),   # If x is a human
        ],[                             # then
        Fact('mortal', name=Var('x'))]) # x is mortal

If there is a fact Fact('human', name='Socrates') on WM, then a new item 
Fact('mortal', name='Socrates') is appended on WM.

Agent
^^^^^
An agent has initial facts and rules. Initial facts are loaded on WM when the
agents are created.

.. sourcecode:: python

    Agent(
        # initial facts
        [ Fact('human', name='Socrates') ], # A human whose name is 'Socrates'
        # rules
        [ Rule('socrates-mortal', [         # A 'socrates-mortal' rule:
            Fact('human', name=Var('x')),   # If x is a human,
            ],[                             # then
            Fact('mortal', name=Var('x'))]) # x is mortal.
        ])


.. automodule:: defworld

   .. toctree::
      :maxdepth: 2

      defworld/agent
      defworld/basic
      defworld/exception
      defworld/patterns
      defworld/rete/node
