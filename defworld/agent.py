""":mod:`defworld.agent` --- Agents
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
from .patterns import Entity, Fact
from .rete.node import Kind, Memory, RuleNode

__all__ = 'Agent', 'WorkingMemory'


class WorkingMemory(object):
    def __init__(self, initial_facts=[]):
        self.facts = list(initial_facts)
        self.entities = {fact.name: fact for fact in initial_facts
            if isinstance(fact, Entity)}

        self.appended = list(initial_facts)
        self.removed = set([])

    def append(self, item):
        if isinstance(item, Entity):
            if item.name in self.entities:
                old_item = self.entities[item.name]
                item = item.apply(old_item)
                self.remove(old_item)

            self.entities[item.name] = item

        self.facts.append(item)
        self.appended.append(item)

    def remove(self, item):
        self.facts.remove(item)
        if isinstance(item, Entity):
            del self.entities[item.name]
        self.removed.add(item)

        try:
            self.appended.remove(item)
        except ValueError:
            pass

    def clear(self):
        self.facts = []
        self.entries = {}

        self.appended = []
        self.removed = set([])


class Agent(object):
    def __init__(self, initial_facts, rules):
        self.rules = rules
        self.agenda = Memory()
        self.new_agenda = []

        # load initial facts onto WM
        self.wm = WorkingMemory(initial_facts)

        self.kinds = Kind(self.wm)

        # build a RETE network
        for rule in self.rules:
            left = None
            for pattern in rule.patterns:
                left = pattern.rete(self.kinds, left)

            left.connect(RuleNode(left, self, rule))
        self.nodes = self.linearize()

    def linearize(self):
        """ Return linearized rete tree """
        candidates = [self.kinds.root]
        nodes = []
        while candidates:
            node = candidates.pop(0)
            nodes.append(node)
            for node in node.post_nodes:
                if node not in candidates:
                    candidates.append(node)
        return nodes

    def clear(self):
        self.agenda.clear()

        for alpha in self.alphas:
            alpha.clear()

        for beta in self.betas:
            beta.clear()

    def match(self):
        for node in self.nodes:
            node.activate()

        self.wm.appended = []
        self.wm.removed.clear()
        return self.agenda

    def activate(self):
        try:
            rule_context = self.agenda[-1]
            for change in rule_context.changes:
                if isinstance(change, Fact):
                    self.wm.append(change)
            return rule_context

        except IndexError:
            return None

    def run(self, max_iter=3):
        log = []
        i = 0
        while True:
            self.match()
            rule = self.activate()

            if rule:
                log.append(rule)
            else:
                break

            i += 1
            if i >= max_iter:
                break

        return log
