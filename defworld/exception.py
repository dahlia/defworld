""":mod:`defworld.exception` --- Errors
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""

class DoesNotMatch(Exception):
    pass


class DiffFactName(Exception):
    """ raised when trying to match facts whose names are different.
    To prevent Not test catching the exception. """
    pass
