""":mod:`defworld.patterns` --- Patterns
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
from .basic import Context, Item, Val, Var
from .exception import DoesNotMatch, DiffFactName
from .rete.node import AlphaNode, BetaNode, NotNode, OrBetaNode


class ConditionalElement(object):
    def default_item(self, other=None):
        if other is None:
            other = self
        return Item(Context(), set([other]))

    def match(self, other=None, item=None):
        """ Return pattern matching """
        if not item:
            item = self.default_item(other)
        else:
            item = item.copy()

        return self._match(other, item)

    def _match(self, other, item):
        if isinstance(other, Val):
            if other.has_value:
                other = other.value
            else:
                raise DoesNotMatch()

        if not isinstance(other, Fact):
            raise DiffFactName()

        if not substitute(self.name, item.context) == other.name:
            raise DiffFactName()

        for key, value in self.items():
            if isinstance(value, Fact):
                try:
                    new_item = value.match(other[key], item)
                    item = item.join(new_item)
                except DiffFactName:
                    raise DoesNotMatch()
            else:
                if isinstance(value, Var):
                    if key in other:
                        value = substitute(value, item.context)
                    else:
                        raise DoesNotMatch(
                            '{0!r} does not have {1!r}'.format(other, key)
                        )
                if value != other[key]:
                    raise DoesNotMatch()

        return item

    def rete(self, kinds, left):
        if left is None:
            return kinds.add(self)
        else:
            return self._rete(kinds, left)

    def _rete(self, kinds, left):
        right = kinds.add(self)
        left = BetaNode(left, right)
        return left

    def is_named(self):
        return False


def substitute(x, context):
    try:
        return x.substitute(context)
    except AttributeError:
        return x


class Fact(ConditionalElement):
    def __init__(self, _name, *args, **kwargs):
        self.name = _name
        ConditionalElement.__init__(self)

        self.ordered = list(args)
        self.slots = kwargs

    def __getitem__(self, key):
        if isinstance(key, int):
            try:
                return self.ordered[key]
            except IndexError:
                return None
        else:
            return self.slots.get(key, None)

    def __setitem__(self, key, value):
        if isinstance(key, int):
            try:
                self.ordered[key] = value
            except IndexError:
                pass
        else:
            self.slots[key] = value

    def items(self):
        ordered_item = zip(range(len(self.ordered)), self.ordered)
        slot_item = self.slots.items()
        return ordered_item + slot_item

    def __iter__(self):
        return iter(range(len(self.ordered)) + self.slots.keys())

    def __repr__(self):
        return repr(self.name) + repr(self.ordered) + repr(self.slots)

    def __hash__(self):
        return hash(repr(self))

    def __eq__(self, other):
        return self.name == other.name and self.ordered == other.ordered and \
            self.slots == other.slots

    def copy(self):
        return type(self)(self.name, *self.ordered, **self.slots)

    def substitute(self, context):
        fact = self.copy()

        for key, value in self.items():
            fact[key] = substitute(value, context)
        return fact

    def apply(self, entity):
        entity = entity.copy()

        for key, value in self.items():
            if isinstance(value, Operator):
                entity[key] = value.apply(entity[key])
            else:
                entity[key] = value
        return entity

    def is_named(self):
        return isinstance(self.name, basestring)


class Entity(Fact):
    pass

class Template(object):
    def __init__(self, name, kind=Fact):
        self.name = name
        self.kind = kind

    def __call__(self, *args, **kwargs):
        return self.kind(self.name, *args, **kwargs)

class Operator(object):
    def __init__(self, value):
        self.value = value

    def __eq__(self, other):
        return self.__class__ == other.__class__ and self.value == other.value

    def __ne__(self, other):
        return not self == other

    def update(self, value):
        return type(self)(value)

    def substitute(self, context):
        if isinstance(self.value, Var):
            v = context[self.value]
            return self.update(v)
        return self

class Include(Operator):
    def __eq__(self, other):
        if isinstance(other, list):
            return self.value in other
        else:
            return super(Include, self).__eq__(other)

    def apply(self, seq):
        return seq + [self.value]

    def __repr__(self):
        return 'Include(%s)' % repr(self.value)

class Exclude(Operator):
    def __eq__(self, other):
        if isinstance(other, list):
            return self.value not in other
        else:
            return super(Exclude, self).__eq__(other)

    def apply(self, seq):
        return [v for v in seq if v != self.value]

    def __repr__(self):
        return 'Exclude(%s)' % repr(self.value)


class Rule(object):
    def __init__(self, name, patterns, changes):
        self.name = name
        self.patterns = patterns
        self.changes = changes

    def __repr__(self):
        return self.name


class Test(ConditionalElement):
    def __init__(self, pattern):
        self.pattern = pattern
        super(Test, self).__init__()

    def _match(self, other=None, item=None):
        self.pattern.match(other, item)
        return item

    def _rete(self, kinds, left):
        right = AlphaNode(left, self)
        return right


class Comparison(Test):
    def __init__(self, x, y):
        self.x = x
        self.y = y
        super(Comparison, self).__init__(self)

    def _match(self, other, item):
        x = substitute(self.x, item.context)
        y = substitute(self.y, item.context)
        if self.compare(x, y):
            return item
        else:
            raise DoesNotMatch

    def copy(self):
        return type(self)(self.x, self.y)

    def __repr__(self):
        cls = type(self)
        return '{0}.{1}({2!r}, {3!r})'.format(cls.__module__, cls.__name__,
                                              self.x, self.y)


class Equal(Comparison):
    def compare(self, x, y):
        return x == y


class And(ConditionalElement):
    def __init__(self, *args):
        self.patterns = args

    def _match(self, other, item):
        for pattern in self.patterns:
            item = pattern.match(other, item)
        return item

    def rete(self, kinds, left):
        for pattern in self.patterns:
            left = pattern.rete(kinds, left)
        return left

    def __repr__(self):
        return 'And({0})'.format(','.join(repr(p) for p in self.patterns))


class Or(ConditionalElement):
    def __init__(self, *args):
        self.patterns = args

    def _match(self, other, item):
        for pattern in self.patterns:
            try:
                item = pattern.match(other, item)
                return item
            except DoesNotMatch:
                pass
            except DiffFactName:
                pass

        raise DoesNotMatch

    def rete(self, kinds, left):
        alphas = []
        for pattern in self.patterns:
            alphas.append(pattern.rete(kinds, left))
        return OrBetaNode(alphas)


class Capsule(ConditionalElement):
    def __init__(self, pattern):
        self.pattern = pattern
        super(Capsule, self).__init__()

    def is_named(self):
        return self.pattern.is_named()

    @property
    def name(self):
        return self.pattern.name

    def copy(self):
        return  type(self)(self.pattern)


class Not(Capsule):
    def _match(self, other=None, item=None):
        try:
            item = self.pattern.match(other, item)
        except DoesNotMatch:
            item.context.clear()
            return item
        else:
            raise DoesNotMatch

    def rete(self, kinds, left):
        right = self.pattern.rete(kinds, left)

        if isinstance(self.pattern, Comparison):
            minimum = 0
        else:
            minimum = -1

        if left is None:
            if self.is_named():
                left = kinds.fact_nodes[self.name]
            else:
                left = kinds.root
        left = NotNode(left, right, minimum)
        return left

    def __repr__(self):
        return 'Not({0})'.format(repr(self.pattern))


def Exist(*patterns):
    return Not(Not(And(*patterns)))

def ForAll(*patterns):
    n = len(patterns)
    if n % 2 == 1:
        k = 2
        p = Not(And(*patterns[n-k:]))
    else:
        k = 1
        p = Not(patterns[n-k])

    for i in range(k+1, n+1):
        p = Not(And(patterns[n-i], p))

    return p


class Func(object):
    def __init__(self, func):
        self.func = func

    def __call__(self, *args, **kwargs):
        return Call(self.func, (args, kwargs))


class Call(object):
    def __init__(self, _func, (args, kwargs)):
        self.func = _func
        self.args = args
        self.kwargs = kwargs

    def substitute(self, context):
        args = [substitute(x, context) for x in self.args]
        kwargs = {k:substitute(v, context) for k, v in self.kwargs}
        return self.func(*args, **kwargs)
