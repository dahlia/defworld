try:
    from setuptools import setup
except ImportError:
    from distribute_setup import use_setuptools
    use_setuptools()
    from setuptools import setup
from setuptools.command.test import test as TestCommand

from defworld.version import VERSION


class test(TestCommand):

    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        from pytest import main
        main(self.test_args)


setup(
    name='Defworld',
    version=VERSION,
    packages=['defworld'],
    description='Defworld is a multi-agent simulation library for '
                'interactive storytelling.',
    author='Jae-Myoung Yu',
    author_email='euphoris' '@' 'gmail.com',
    url='https://bitbucket.org/euphoris/defworld',
    tests_require=['pytest'],
    cmdclass={'test': test}
)
