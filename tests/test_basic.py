import pytest

from defworld.agent import WorkingMemory
from defworld.basic import Context, Item, Var
from defworld.constraints import Typed
from defworld.patterns import (And, Equal, Exclude, Fact, Func, Include, Not,
                               Or, Template)
from defworld.exception import DoesNotMatch
from defworld.rete.node import Kind, AlphaNode, FactNode

from .rules import monkey


def test_none():
    monkey_ = Fact('monkey')
    assert not monkey_['has']

def test_match():
    item = monkey(at='door').match(monkey(at='door', on='floor'))
    assert item.context == {}
    item = monkey(at='door', on='floor').match(monkey(at='door', on='floor'))
    assert item.context == {}


def test_match_ordered_slot():
    socrates = Fact('human', 'Socrates')
    pattern = Fact('human', Var('who'))
    item = pattern.match(socrates)
    assert item.context[Var('who')] == 'Socrates'


def test_not_match():
    with pytest.raises(DoesNotMatch):
        monkey(at='door').match(monkey(at='bananas', on='floor'))
    with pytest.raises(DoesNotMatch):
        monkey(at='door', on='floor').match(monkey(at='bananas', on='floor'))

def test_apply():
    result = monkey(at='door').apply(monkey(at='bananas', on='floor'))
    assert result['at'] == 'door'
    assert result['on'] == 'floor'

def test_var():
    # True wherever monkey is
    assert monkey(at=Var('x')).match(monkey(at='door'))

def test_var_same():
    # True if monkey has something and eat it
    pattern = monkey(has=Var('x'), eat=Var('x'))
    fact = monkey(has='bananas', eat='bananas')
    item = pattern.match(fact)
    assert item.context[Var('x')] == 'bananas'

def test_var_diff():
    # True if monkey has something and eat it
    pattern = monkey(has=Var('x'), eat=Var('x'))
    fact = monkey(has='apple', eat='bananas')
    with pytest.raises(DoesNotMatch):
        pattern.match(fact)


def test_var_constraint():
    pattern = Fact('data', Var('x', Typed(int)))
    pattern.match(Fact('data', 1))

    with pytest.raises(DoesNotMatch):
        pattern.match(Fact('data', 'a'))


def test_exclude():
    e = Exclude('a')
    assert e == ['b','c']
    assert e.apply(['a','b']) == ['b']

def test_include():
    i = Include('b')
    assert i == ['a','b']
    assert i.apply(['a']) == ['a','b']

def test_equal_operator():
    i = Include('b')
    j = Include('b')
    assert i == j

    i = Exclude('b')
    j = Exclude('b')
    assert i == j

def test_unequal_operator():
    i = Include('a')
    j = Exclude('b')
    assert i != j

    i = Include('a')
    j = Include('b')
    assert i != j

    i = Exclude('a')
    j = Include('b')
    assert i != j


def test_fact_name_var():
    item = Fact(Var('x')).match(Fact('hello'))
    assert item.context[Var('x')] == 'hello'


def test_equal():
    context = Context()
    context[Var('x')] = 1
    item = Item(context, set())
    Equal(Var('x'), 1).match(item=item)


def test_and():
    data = Template('data')
    and_pattern = And(data(a=1), data(b=2))

    and_pattern.match(data(a=1, b=2))

    with pytest.raises(DoesNotMatch):
        and_pattern.match(data(a=1))

    with pytest.raises(DoesNotMatch):
        and_pattern.match(data(b=2))


def test_or():
    data = Template('data')
    or_pattern = Or(data(a=1), data(a=2))

    or_pattern.match(data(a=1))
    or_pattern.match(data(a=2))

    with pytest.raises(DoesNotMatch):
        or_pattern.match(data(a=3))


def test_not():
    item = Not(Fact('data', a=1)).match(Fact('data', a=2))
    assert item.context == {}

    with pytest.raises(DoesNotMatch):
        Not(Fact('data', a=1)).match(Fact('data', a=1))


def test_not_equal():
    context = Context()
    context[Var('x')] = 1
    item = Item(context, set())
    Not(Equal(Var('x'), 2)).match(item=item)

    item = Fact('boy', loves=Var('x')).match(Fact('boy', loves='yandere'))
    with pytest.raises(DoesNotMatch):
        Not(Equal(Var('x'), 'yandere')).match(item=item)


def test_not_with_var():
    fact_x = Fact('data', a=Var('x'))
    fact_2 = Fact('data', a=2)

    with pytest.raises(DoesNotMatch):
        Not(fact_x).match(fact_2)

    item_x_1 = fact_x.match(Fact('data', a=1))
    Not(fact_x).match(fact_2, item_x_1)

    not_same = Not(Fact('data', a=Var('x'), b=Var('x')))
    with pytest.raises(DoesNotMatch):
         not_same.match(Fact('data', a=1, b=1))


def test_double_not():
    pattern = Not(Not(Fact('data', a=1)))
    assert pattern.is_named()
    assert pattern.name == 'data'


def test_negation_rete():
    pattern = Not(Not(And(Fact('data', a=2), Fact('proc', b=3))))
    kinds = Kind(WorkingMemory())
    terminal = pattern.rete(kinds, None)
    assert isinstance(terminal.left, FactNode)

    assert len(kinds.fact_nodes) == 2


def test_ce_rete():
    kinds = Kind(WorkingMemory())
    pattern = Fact('data', a=1)
    terminal = pattern.rete(kinds, None)
    assert isinstance(terminal, AlphaNode)


def test_func():
    x = Var('x')
    y = Var('y')

    inc = Func(lambda n: n+1)
    call = inc(x)
    context = Context()
    context[x] = 2
    assert call.substitute(context) == 3

    context = Context()
    context[x] = 1
    context[y] = 1

    item = Item(context, set())
    neq = Equal(x, inc(x))

    with pytest.raises(DoesNotMatch):
        neq.match(item=item)

    eq = Equal(inc(x), inc(y))
    assert eq.match(item=item)

    @Func
    def dec(n):
        return n-1

    Equal(dec(x), 0).match(item=item)


def test_nested_match():
    pattern = Fact('outer', v=Fact('inner', v=Var('x')))
    value = Fact('outer', v=Fact('inner', v=3))
    item = pattern.match(value)
    assert item.context[Var('x')] == 3
