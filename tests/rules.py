from defworld.patterns import (Entity, Equal, Exclude, Include, Not, Rule,
                               Template, Var)


son = Template('son', Entity)
car = Template('car', Entity)
shop = Template('shop', Entity)
me = Template('me', Entity)
school_rules =  [
    Rule('drive-son-to-school', [
        son(at='home'),
        car(needs=None)
        ],[
        son(at='school')]),

    Rule('shop-installs-battery',[
        car(needs='battery'),
        shop(knows='problem', has='money'),
        ],[
        car(needs=None)]),

    Rule('tell-shop-problem',[
        me(in_communication_with_shop=True),
        ],[
        shop(knows='problem')]),

    Rule('telephone-shop',[
        me(know='phone-number'),
        ],[
        me(in_communication_with_shop=True)]),

    Rule('look-up-number',[
        me(have=Include('phone_book')),
        ],[
        me(know='phone-number')]),

    Rule('give-shop-money',[
        shop(knows='problem'),
        me(have=Include('money')),
        ],[
        shop(has='money'), me(have=Exclude('money'))])
    ]

monkey = Template('monkey', Entity)
chair = Template('chair', Entity)
monkey_rules = [
    Rule('climb-on-chair',[
        chair(at='middle-room'),
        monkey(at='middle-room', on='floor'),
        ],[
        monkey(at='bananas', on='chair')]),

    Rule('push-chair-from-door-to-middle-room',[
        chair(at='door'),
        monkey(at='door'),
        ],[
        chair(at='middle-room'),
        monkey(at='middle-room')]),

    Rule('walk-from-door-to-middle-room',[
        monkey(at='door', on='floor'),
        ],[
        monkey(at='middle-room')]),

    Rule('grasp-bananas',[
        monkey(at='bananas', has=None),
        ],[
        monkey(has='bananas')]),

    Rule('drop-ball',[
        monkey(has='ball'),
        ],[
        monkey(has=None)]),

    Rule('eat-bananas',[
        monkey(has='bananas', hungry=True),
        ],[
        monkey(has=None, hungry=False)])
    ]


yandere = Template('yandere', Entity)
boy = Template('boy', Entity)
give = Template('give')
yandere_rules = [
    Rule('this-is-not-for-you',[
        yandere(has=Var('something')),
        boy(has=None),
        ],[
        give(from_='yandere', to_='boy', what=Var('something'))]),
    Rule('hate-list',[
        boy(loves=Var('someone')),
        Not(Equal(Var('someone'), 'yandere'))
        ],[
        yandere(hates=Include(Var('someone')))]),
    Rule('nice-boat',[
        Entity(Var('someone'), loves='boy'),
        ],[
        yandere(hates=Include(Var('someone')))])
    ]
